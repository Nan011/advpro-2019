package id.ac.ui.cs.advprog.tutorial1.strategy;

import id.ac.ui.cs.advprog.tutorial1.strategy.FlyBehavior;

public class FlyNoWay implements FlyBehavior {
    @Override
    public void fly() {
        System.out.println("Whyy?");
    }
}
