package id.ac.ui.cs.advprog.tutorial1.strategy;

import id.ac.ui.cs.advprog.tutorial1.strategy.QuackBehavior;

public class Squeak implements QuackBehavior {
    @Override
    public void quack() {
        System.out.println("Chiit!");
    }
}
