package id.ac.ui.cs.advprog.tutorial1.strategy;

import  id.ac.ui.cs.advprog.tutorial1.strategy.MallardDuck;
import  id.ac.ui.cs.advprog.tutorial1.strategy.ModelDuck;
import  id.ac.ui.cs.advprog.tutorial1.strategy.FlyRocketPowered;

public class MiniDuckSimulator {

    public static void main(String[] args) {
        Duck mallard = new MallardDuck();
        mallard.performQuack();
        mallard.performFly();

        Duck model = new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }
}
