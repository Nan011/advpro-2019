package id.ac.ui.cs.advprog.tutorial1.strategy;

import  id.ac.ui.cs.advprog.tutorial1.strategy.Quack;
import  id.ac.ui.cs.advprog.tutorial1.strategy.FlyWithWings; 

public class MallardDuck extends Duck {
    public MallardDuck() {
        this.quackBehavior = new Quack();
        this.flyBehavior = new FlyWithWings();
    }

    @Override
    public void display() {
        System.out.println("MallardDuck");
    }
}
