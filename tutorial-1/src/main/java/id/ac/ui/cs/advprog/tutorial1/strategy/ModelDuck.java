package id.ac.ui.cs.advprog.tutorial1.strategy;

import  id.ac.ui.cs.advprog.tutorial1.strategy.Squeak; 
import  id.ac.ui.cs.advprog.tutorial1.strategy.FlyNoWay; 

public class ModelDuck extends Duck {
    public ModelDuck() {
        super.quackBehavior = new Squeak();
        super.flyBehavior = new FlyNoWay();
    }

    @Override
    public void display() {
        System.out.println("ModelDuck");
    }
}
