package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import java.lang.IllegalArgumentException;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class SecurityExpert extends Employees {
    public static final double MIN_SALARY = 70000.00;
    public SecurityExpert(String name, double salary) {
        super(name, salaryValidation(salary));
        this.role = "Security Expert";
    }
    
    private static double salaryValidation(double salary) {
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException("SecurityExpert Salary must not lower than 70000.00");
        } else {
            return salary;
        }
    }
}
