package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import java.lang.IllegalArgumentException;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    public static final double MIN_SALARY = 50000.00;
    public NetworkExpert(String name, double salary) {
        super(name, salaryValidation(salary));
        this.role = "Network Expert";
    }
    
    private static double salaryValidation(double salary) {
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException("NetworkExpert Salary must not lower than 50000.00");
        } else {
            return salary;
        }
    }
}
