package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import java.lang.IllegalArgumentException;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    public static final double MIN_SALARY = 90000.00;
    public UiUxDesigner(String name, double salary) {
        super(name, salaryValidation(salary));
        this.role = "UI/UX Designer";
    }
    
    private static double salaryValidation(double salary) {
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException("UiUxDesigner Salary must not lower than 90000.00");
        } else {
            return salary;
        }
    }
}
