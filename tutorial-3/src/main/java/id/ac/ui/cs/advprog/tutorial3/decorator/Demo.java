package id.ac.ui.cs.advprog.tutorial3.decorator; 

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer; 
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator; 

public class Demo { 
    public static void main(String[] args) { 
        Food mySandwich = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled(); 
        System.out.println(mySandwich.getDescription()); 

        mySandwich = FillingDecorator.CHEESE.addFillingToBread(mySandwich); 
        mySandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(mySandwich); 
        System.out.println(mySandwich.getDescription()); 
    } 
}