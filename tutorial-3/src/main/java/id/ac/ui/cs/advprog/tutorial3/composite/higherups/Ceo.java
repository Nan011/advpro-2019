package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import java.lang.IllegalArgumentException;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public static final double MIN_SALARY = 200000;
    public Ceo(String name, double salary) {
        super(name, salaryValidation(salary));
        this.role = "CEO";
    }

    private static double salaryValidation(double salary) {
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException("CEO Salary must not lower than 200000.00");
        } else {
            return salary;
        }
    }
}
