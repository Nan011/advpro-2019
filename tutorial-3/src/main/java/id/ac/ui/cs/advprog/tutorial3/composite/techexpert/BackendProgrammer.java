package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import java.lang.IllegalArgumentException;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class BackendProgrammer extends Employees {
    public static final double MIN_SALARY = 20000.00;
    public BackendProgrammer(String name, double salary) {
        super(name, salaryValidation(salary));
        this.role = "Back End Programmer";
    }
    
    private static double salaryValidation(double salary) {
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException("BackendProgrammer Salary must not lower than 20000.00");        } else {
            return salary;
        }
    }
}
