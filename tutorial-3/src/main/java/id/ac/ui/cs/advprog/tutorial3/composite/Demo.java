package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Company;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class Demo {
    public static void main(String[] args) {
        Company company = new Company();
        BackendProgrammer backendProgrammer = new BackendProgrammer("Chris", 20000);
        FrontendProgrammer frontendProgrammer = new FrontendProgrammer("Lisa", 30000);
        Ceo ceo = new Ceo("Haise", 100000);
        
        company.addEmployee(ceo);
        System.out.println("Added CEO");
        System.out.format("CEO's salary: %f\n", ceo.getSalary());
        System.out.format("Company's net salaries: %f\n", company.getNetSalaries());

        company.addEmployee(backendProgrammer);
        company.addEmployee(frontendProgrammer);
        System.out.println("Added frontend and backend programmer");
        System.out.format("Frontend programmer's salary: %f\n", frontendProgrammer.getSalary());
        System.out.format("Backend programmer's salary: %f\n", backendProgrammer.getSalary());
        System.out.format("Company's net salaries: %f\n", company.getNetSalaries());
    }
}
