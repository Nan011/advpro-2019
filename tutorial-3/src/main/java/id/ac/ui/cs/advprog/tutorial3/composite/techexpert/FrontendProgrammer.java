package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import java.lang.IllegalArgumentException;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class FrontendProgrammer extends Employees {
    public static final double MIN_SALARY = 30000.00;
    public FrontendProgrammer(String name, double salary) {
        super(name, salaryValidation(salary));
        this.role = "Front End Programmer";
    }
    
    private static double salaryValidation(double salary) {
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException("FrontendProgrammer Salary must not lower than 30000.00");
        } else {
            return salary;
        }
    }
}
