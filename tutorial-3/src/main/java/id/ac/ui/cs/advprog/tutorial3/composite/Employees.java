package id.ac.ui.cs.advprog.tutorial3.composite;

public abstract class Employees {
    protected String name = "Unidentified Name";
    protected double salary;
    protected String role;

    public Employees(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return this.name;
    }

    public double getSalary() {
        return this.salary;
    }

    public String getRole() {
        return this.role;
    }
}
