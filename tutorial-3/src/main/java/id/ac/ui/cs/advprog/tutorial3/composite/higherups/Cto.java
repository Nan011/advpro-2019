package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import java.lang.IllegalArgumentException;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public static final double MIN_SALARY = 100000.00;
    public Cto(String name, double salary) {
        super(name, salaryValidation(salary));
        this.role = "CTO";
    }
    
    private static double salaryValidation(double salary) {
        if (salary < MIN_SALARY) {
            throw new IllegalArgumentException("CTO Salary must not lower than 100000.00");
        } else {
            return salary;
        }
    }
}
